/*
 * lna.h
 *
 *  Created on: Sep 6, 2019
 *      Author: Alex Orange
 */

#ifndef LNA_H_
#define LNA_H_



void setup_lna();
void set_lna_power_state(uint_fast8_t on);
void set_lna_voltage(uint_fast8_t high);
uint_fast8_t get_lna_power_good();
void set_lna_bias(uint_fast8_t bias);


#endif /* LNA_H_ */
