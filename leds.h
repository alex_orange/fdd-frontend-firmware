/*
 * leds.h
 *
 *  Created on: Nov 5, 2019
 *      Author: alex
 */

#ifndef LEDS_H_
#define LEDS_H_

void setup_leds();
void set_led_state(uint_fast8_t led, uint_fast8_t on);

#endif /* LEDS_H_ */
