/*
 * mac_addr.h
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */

#ifndef MAC_ADDR_H_
#define MAC_ADDR_H_

void setup_mac_addr();
void get_mac_addr();

#endif /* MAC_ADDR_H_ */
