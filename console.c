/*
 * console.c
 *
 *  Created on: Sep 8, 2019
 *      Author: Alex Orange
 */

#include <stdint.h>

#include "lna.h"
#include "pa.h"
#include "leds.h"
#include "attenuator.h"

#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/slnetif/slnetifndk.h>
#include <ti/net/slnetif.h>


void console_task(SOCKET connection_socket, uintptr_t unused) {
    char recv_queue[1];
    int recv_size;
    char num;
    char num2;

    while((recv_size = NDK_recv(connection_socket, recv_queue,
                                sizeof(recv_queue), 0)) > 0) {
        switch(recv_queue[0]) {
            case 'l':
                set_lna_power_state(num & 1);
                set_lna_voltage(num & 2);
                break;
            case 'L':
                set_lna_bias(num);
                break;
            case 'i':
                set_led_state(num & 0x3, num & 0x4);
                break;
            case 'p':
                set_pa_power_state(num & 1);
                set_pa_voltage(num & 2);
                break;
            case 'P':
                set_pa_enable(num);
                break;
            case 'x':
                num2 = num;
                break;
            case 't':
                set_attenuator(0, num | num2 << 4);
                break;
            case 'T':
                set_attenuator(1, num | num2 << 4);
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                num = recv_queue[0] - '0';
                break;
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
                num = recv_queue[0] - 'W';
                break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                num = recv_queue[0] - '7';
                break;
        }

        NDK_send(connection_socket, recv_queue, recv_size, 0);
    }
}
