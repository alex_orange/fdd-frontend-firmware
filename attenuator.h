/*
 * attenuator.h
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */

#ifndef ATTENUATOR_H_
#define ATTENUATOR_H_

void setup_attenuator();
void set_attenuator(uint_fast8_t rx, uint_fast8_t value);


#endif /* ATTENUATOR_H_ */
