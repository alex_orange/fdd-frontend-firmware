/*
 * temperature.h
 *
 *  Created on: Sep 5, 2019
 *      Author: crazycasta
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_


#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/slnetif/slnetifndk.h>
#include <ti/net/slnetif.h>


void setup_temperature();
float get_temperature_celsius();
void temperature_thread(SOCKET connection_socket, uintptr_t unused);


#endif /* TEMPERATURE_H_ */
