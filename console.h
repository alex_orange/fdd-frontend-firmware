/*
 * console.h
 *
 *  Created on: Sep 8, 2019
 *      Author: alex
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

void console_task(SOCKET connection_socket, uintptr_t unused);

#endif /* CONSOLE_H_ */
