/*
 * leds.c
 *
 *  Created on: November 5, 2019
 *      Author: Alex Orange
 */

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>


void setup_leds() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

    // LED GPIO
    GPIODirModeSet(GPIO_PORTC_BASE, 0xf0, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTC_BASE, 0xf0, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTC_BASE, 0x20, 0x20);
}

void set_led_state(uint_fast8_t led, uint_fast8_t on) {
    switch(led) {
        case 0:
            GPIOPinWrite(GPIO_PORTC_BASE, 0x10, on ? 0x10 : 0x00);
            break;
        case 1:
            GPIOPinWrite(GPIO_PORTC_BASE, 0x20, on ? 0x20 : 0x00);
            break;
        case 2:
            GPIOPinWrite(GPIO_PORTC_BASE, 0x40, on ? 0x40 : 0x00);
            break;
        case 3:
            GPIOPinWrite(GPIO_PORTC_BASE, 0x80, on ? 0x80 : 0x00);
            break;
    }
}
