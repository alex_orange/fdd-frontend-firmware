/*
 * frontend_board.c
 *
 *  Created on: Sep 5, 2019
 *      Author: Alex Orange
 */


#include <stdint.h>
#include <stdlib.h>

#ifndef __MSP432E401Y__
#define __MSP432E401Y__
#endif

#include <ti/devices/msp432e4/inc/msp432.h>

#include <ti/devices/msp432e4/driverlib/adc.h>
#include <ti/devices/msp432e4/driverlib/interrupt.h>
#include <ti/devices/msp432e4/driverlib/pwm.h>
#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/udma.h>

#include <ti/drivers/Power.h>


/*
 *  =============================== EMAC ===============================
 */
/* minimize scope of ndk/inc/stk_main.h -- this eliminates TIPOSIX path dependencies */
#define NDK_NOUSERAPIS 1
#include <ti/drivers/emac/EMACMSP432E4.h>

/*
 *  Required by the Networking Stack (NDK). This array must be NULL terminated.
 *  This can be removed if NDK is not used.
 *  Double curly braces are needed to avoid GCC bug #944572
 *  https://bugs.launchpad.net/gcc-linaro/+bug/944572
 */
NIMU_DEVICE_TABLE_ENTRY NIMUDeviceTable[2] = {
    {
        /* Default: use Ethernet driver */
        .init = EMACMSP432E4_NIMUInit
    },
    {NULL}
};

/*
 *  Ethernet MAC address
 *  NOTE: By default (i.e. when each octet is 0xff), the driver reads the MAC
 *        address that's stored in flash. To override this behavior, manually
 *        set the octets of the MAC address you wish to use into the array here:
 */
//unsigned char macAddress[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
//unsigned char macAddress[6] = {0xde, 0xad, 0xc0, 0xde, 0xba, 0xdd};
//unsigned char macAddress[6] = {0xde, 0xad, 0xbe, 0xef, 0xca, 0xfe};
//unsigned char macAddress[6] = {0xfa, 0xce, 0xde, 0xaf, 0xfa, 0xde};
//unsigned char macAddress[6] = {0x70, 0xff, 0x76, 0x1c, 0xd2, 0x8a};
//unsigned char macAddress[6] = {0xde, 0xca, 0xde, 0xda, 0xbb, 0x1e};
unsigned char macAddress[6] = {0xfe, 0xed, 0xfa, 0xce, 0xca, 0xfe};

/* EMAC configuration structure */
const EMACMSP432E4_HWAttrs EMACMSP432E4_hwAttrs = {
    .baseAddr = EMAC0_BASE,
    .intNum = INT_EMAC0,
    .intPriority = (~0),
    .led0Pin = EMACMSP432E4_PF0_EN0LED0,
    .led1Pin = EMACMSP432E4_PF4_EN0LED1,
    .macAddress = macAddress
};

